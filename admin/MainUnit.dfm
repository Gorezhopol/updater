object MainForm: TMainForm
  Left = 659
  Top = 100
  BorderStyle = bsSingle
  Caption = #1050#1083#1080#1077#1085#1090' '#1072#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088#1072
  ClientHeight = 147
  ClientWidth = 286
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object AddressLabel: TLabel
    Left = 8
    Top = 8
    Width = 41
    Height = 13
    Caption = 'ip-'#1072#1076#1088#1077#1089
  end
  object PasswordLabel: TLabel
    Left = 152
    Top = 8
    Width = 38
    Height = 13
    Caption = #1055#1072#1088#1086#1083#1100
  end
  object StatusLabel: TLabel
    Left = 8
    Top = 120
    Width = 205
    Height = 13
    Caption = #1057#1086#1077#1076#1080#1085#1077#1085#1080#1077' '#1089' '#1089#1077#1088#1074#1077#1088#1086#1084' '#1085#1077' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085#1086
  end
  object PortLabel: TLabel
    Left = 104
    Top = 8
    Width = 25
    Height = 13
    Caption = #1055#1086#1088#1090
  end
  object ConnectButton: TButton
    Left = 8
    Top = 56
    Width = 89
    Height = 25
    Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100#1089#1103
    TabOrder = 3
    OnClick = ConnectButtonClick
  end
  object AddressEdit: TEdit
    Left = 8
    Top = 24
    Width = 89
    Height = 21
    TabOrder = 0
  end
  object PasswordEdit: TEdit
    Left = 152
    Top = 24
    Width = 121
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
    Text = 'password'
  end
  object NewButton: TButton
    Left = 8
    Top = 88
    Width = 137
    Height = 25
    Caption = #1055#1077#1088#1077#1076#1072#1090#1100' '#1085#1086#1074#1099#1081' '#1092#1072#1081#1083'...'
    Enabled = False
    TabOrder = 5
    OnClick = NewButtonClick
  end
  object PortEdit: TEdit
    Left = 104
    Top = 24
    Width = 41
    Height = 21
    TabOrder = 1
  end
  object UpdateButton: TButton
    Left = 104
    Top = 56
    Width = 169
    Height = 25
    Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1092#1072#1081#1083' '#1085#1072' '#1089#1077#1088#1074#1077#1088#1077'...'
    Enabled = False
    TabOrder = 4
    OnClick = UpdateButtonClick
  end
  object SocketC: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = SocketCConnect
    OnDisconnect = SocketCDisconnect
    OnError = SocketCError
    Left = 184
    Top = 88
  end
  object OpenDialog: TOpenDialog
    Filter = #1048#1089#1087#1086#1083#1085#1103#1077#1084#1099#1077' '#1092#1072#1081#1083#1099'|*.exe'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 152
    Top = 88
  end
  object CleanTrashTimer: TTimer
    Interval = 5000
    OnTimer = CleanTrashTimerTimer
    Left = 248
    Top = 65528
  end
end
