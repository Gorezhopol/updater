unit ServerFilesUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TServerFilesWindow = class(TForm)
    FilesListBox: TListBox;
    ChooseButton: TButton;
    procedure ChooseButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ServerFilesWindow: TServerFilesWindow;

implementation

uses MainUnit;

{$R *.dfm}

procedure TServerFilesWindow.ChooseButtonClick(Sender: TObject);
var
  i:integer;
begin
  for i:=0 to FilesListBox.Items.Count-1 do
  begin
    if FilesListBox.selected[i]=true then
    begin
      serverFileName:=FilesListBox.Items.Strings[i];
      ServerFilesWindow.Visible:=false;
      MainForm.sendforupdate;
      break;
    end;
    if i=FilesListBox.Items.Count-1 then
      ShowMessage('�������� ��� ���������� �����');
  end;
end;

end.
