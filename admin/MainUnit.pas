unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, StdCtrls, WinSock, StrUtils, UpdateUnit, ExtCtrls, ServerFilesUnit;

type
  TMainForm = class(TForm)
    SocketC: TClientSocket;
    ConnectButton: TButton;
    AddressEdit: TEdit;
    PasswordEdit: TEdit;
    NewButton: TButton;
    OpenDialog: TOpenDialog;
    AddressLabel: TLabel;
    PasswordLabel: TLabel;
    CleanTrashTimer: TTimer;
    StatusLabel: TLabel;
    PortEdit: TEdit;
    PortLabel: TLabel;
    UpdateButton: TButton;
    procedure ConnectButtonClick(Sender: TObject);
    procedure SocketCConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure NewButtonClick(Sender: TObject);
    procedure SocketCDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure CleanTrashTimerTimer(Sender: TObject);
    procedure SocketCError(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure UpdateButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure sendforupdate;
    function FileInUse(FileName:string): Boolean;
    function FileVersion(const FileName: String): String;
    { Public declarations }
  end;
  TNewThread = class(TThread)
  private
    fs:TFileStream;
    UpdatingF: boolean;
    DataInc,DataNow: Integer;
    procedure ButtonActivate;
    procedure ButtonReactivate;
    procedure ButtonDeactivate;
    procedure FormActivate;
    procedure FormUpdate;
    procedure FormDeactivate;
    procedure Close;
    function FileVersion(const FileName: String): String;
    function GetAppTime:String;
    { Private declarations }
  protected
    procedure Execute; override;
  public
    Socket: TCustomWinSocket;
  end;
    

var
  MainForm: TMainForm;
  uFile: string;
  Thread: TNewThread;
  serverExes: TStringList;
  serverFileName: string;

implementation

{$R *.dfm}

//�����, �������������� ����� ������� ����� ����������������� �������� � ��������

procedure TNewThread.Execute;
var
  str:string;
  Buf: PByte;
  DataLen: Integer;
  n:Integer;
  finished:boolean;
begin
  finished:=false;
  serverExes:=TStringList.Create;
  DataNow:=0;
  UpdatingF:=false;
  uFile:= ExtractFileName(Application.ExeName);
  while Socket.Connected=True do
  begin
    while Socket.ReceiveLength>0 do
      begin
        if UpdatingF=false then  //����� ������ �������
        begin
          str:=Socket.ReceiveText;
          if(StrPos(PAnsiChar(str),PAnsiChar('#FILE#'))<>nil) then  //��������� ������ ��������� ������
          begin
            if(StrPos(PAnsiChar(str),PAnsiChar('#LISTFINISH#'))<>nil) then  //����� ������
            begin
              finished:=true;
              str:=AnsiReplaceText(str,'#LISTFINISH#','');
            end;
            if serverExes.Count<=0 then
            begin
              serverExes.Text:=AnsiReplaceText(str,'#FILE#',#13#10);
              serverExes.Delete(0);
            end
            else
            begin
              n:=serverExes.Count;
              serverExes.Text:=serverExes.text+(AnsiReplaceText(str,'#FILE#',#13#10));
              serverExes.Delete(n);
            end;
            if finished then
              Socket.SendText('#CURRENTAPP#'+uFile+'||' + GetAppTime + '||' + FileVersion(uFile));  //�������� ���������� �� ����������������� ����������
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#LISTFINISH#'))<>nil) then   //����� ������
          begin
            str:=AnsiReplaceText(str,'#LISTFINISH#','');
            Socket.SendText('#CURRENTAPP#'+uFile+'||' + GetAppTime + '||' + FileVersion(uFile));
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#PROCEED#'))<>nil) then   //���������� ���������� �� ���������. ����� ���������� ������
            synchronize(ButtonActivate);
          if(StrPos(PAnsiChar(str),PAnsiChar('#EXISTS#'))<>nil) then
            ShowMessage('������ ��� ����� ���� � ����� ������. ������������ ���������, ���� �������� ��������� ���� �� �������');
          if(StrPos(PAnsiChar(str),PAnsiChar('#UPDATECURRENTAPP#'))<>nil) then   //���������� �������� ����������������� ����������
          begin
            DataInc:=strtoint(AnsiReplaceText(str,'#UPDATECURRENTAPP#',''));
            Socket.SendText('#READY#');
            synchronize(FormActivate);
            UpdatingF:=true;
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#FINISH#'))<>nil) then   //���� ��� ��������� ������
          begin
            synchronize(ButtonReactivate);
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#ALREADYINPROGRESS#'))<>nil) then
          begin
            ShowMessage('������ ��� ���������� �������� ������ ���������');
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#FORBIDDENNAME#'))<>nil) then
          begin
            ShowMessage('������������ ��� ���������. ������������ exe-����');
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#OLDVERSION#'))<>nil) then
          begin
            str:=AnsiReplaceText(str,'#OLDVERSION#','');
            ShowMessage('�������� ����� ��������. �� ������� ��� �������� ����� ����� ������ ���������:' + str);
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#READYTORECEIVE#'))<>nil) then   //������ ����� � �������� �����
          begin
            fs:=TFileStream.Create(uFile,fmOpenRead);
            getMem(Buf,fs.size);
            DataLen:=fs.Size;
            fs.ReadBuffer(buf^,fs.size);
            fs.Free;
            Socket.SendBuf(buf^,DataLen);
            freeMem(Buf);
            uFile:='';
            synchronize(ButtonDeactivate);
          end;
        end
        else   //����� �������� ������
        begin
          n:=Socket.ReceiveLength;
          getMem(Buf,n);
          DataLen:=Socket.ReceiveBuf(Buf^,n);
          DataNow:=DataNow+n;
          if (not FileExists(uFile+'.tmp')) and (DataLen>0) then
          begin
            fs:=TFileStream.Create(uFile+'.tmp',fmCreate or fmOpenWrite);
            fs.WriteBuffer(Buf^,DataLen);
          end
          else
          begin
            fs.WriteBuffer(Buf^,DataLen);
          end;
          FreeMem(Buf,n);
          synchronize(FormUpdate);
          if DataNow>=DataInc then
          begin
            synchronize(FormDeactivate);
            UpdatingF:=false;
            fs.Free;
            RenameFile(uFile,AnsiReplaceText(uFile,'.exe','OLD.exe'));
            RenameFile(uFile+'.tmp',uFile);
            WinExec(PAnsiChar(ExtractFilePath(Application.ExeName)+uFile),0);
            synchronize(Close);
            Terminate;
          end;
        end;
      end;
    sleep(1000);
  end;
  Terminate;
end;

//��������� ������ �������� � ���������� ������ �� �������

procedure TNewThread.ButtonActivate;
begin
  MainForm.NewButton.Enabled:=true;
  MainForm.UpdateButton.Enabled:=true;
  MainForm.StatusLabel.Caption:='���������� � �������� �����������';
end;

//����������� ������ �������� � ���������� ������ �� �������

procedure TNewThread.ButtonDeactivate;
begin
  MainForm.NewButton.Enabled:=false;
  MainForm.UpdateButton.Enabled:=false;
  MainForm.StatusLabel.Caption:='�������� �����...';
end;

//��������� ��������� ������ �������� � ���������� ������ �� �������

procedure TNewThread.ButtonReactivate;
begin
  MainForm.NewButton.Enabled:=true;
  MainForm.UpdateButton.Enabled:=true;
  MainForm.StatusLabel.Caption:='���� ������� �������';
end;

//��������� ���� ���������� ����������

procedure TNewThread.FormActivate;
begin
  UpdateWindow.Enabled:=true;
  UpdateWindow.Visible:=true;
  UpdateWindow.UpdatePB.Max:=DataInc;
  MainForm.StatusLabel.Caption:='����������...';
end;

//���������� ���������� � �������� �������� ���������� ����������

procedure TNewThread.FormUpdate;
begin
  UpdateWindow.UpdatePB.Position:=DataNow;
  UpdateWindow.UpdateLabel.Caption:='���������� ���������:' + inttostr(round((Int(DataNow)/Int(DataInc))*100)) + '%';
end;

//����������� ���� ���������� ����������

procedure TNewThread.FormDeactivate;
begin
  UpdateWindow.Visible:=false;
  UpdateWindow.UpdatePB.Position:=0;
  UpdateWindow.UpdateLabel.Caption:='';
  UpdateWindow.Enabled:=false;
end;

procedure TNewThread.Close;
begin
  MainForm.Close;
end;

//���������� ���� ���������� ��������� ������������������ �������

function TNewThread.GetAppTime:String;
var
  crTime: TWin32FileAttributeData;
  sysTime,localTime: TSystemTime;
begin
  GetFileAttributesEx(PAnsiChar(Application.ExeName), GetFileExInfoStandard,@crTime);
  FileTimeToSystemTime(crTime.ftLastWriteTime,sysTime);
  SystemTimeToTzSpecificLocalTime(nil, sysTime, localTime);
  Result:=DateTimeToStr(SystemTimeToDateTime(localTime));
end;

//���������� ���������� � ������ �����

function TNewThread.FileVersion(const FileName: String): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PAnsiChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PAnsiChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('%d.%d.%d.%d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS), //Minor
              HiWord(dwFileVersionLS), //Release
              LoWord(dwFileVersionLS)]); //Build
      end
      else
        Result := '0.0.0.0';
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
    Result := '0.0.0.0';
end;

function TMainForm.FileVersion(const FileName: String): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PAnsiChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PAnsiChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('%d.%d.%d.%d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS), //Minor
              HiWord(dwFileVersionLS), //Release
              LoWord(dwFileVersionLS)]); //Build
      end
      else
        Result := '0.0.0.0';
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
    Result := '0.0.0.0';
end;

//������������ �� ���� � ������ ������

function TMainForm.FileInUse(FileName: string): Boolean;
var hFileRes: HFILE;
begin
  Result := False;
  if not FileExists(FileName) then exit;
  hFileRes := CreateFile(PAnsiChar(FileName),
                                    GENERIC_READ or GENERIC_WRITE,
                                    0,
                                    nil,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    0);
  Result := (hFileRes = INVALID_HANDLE_VALUE);
  if not Result then
    CloseHandle(HFileRes);
end;

//������������ � �������

procedure TMainForm.ConnectButtonClick(Sender: TObject);
var
  F:TSearchRec;
  port:integer;
begin
  if FindFirst(AnsiReplaceText(Application.ExeName,'.exe','OLD.exe'),faAnyFile,F)=0 then
    DeleteFile(F.Name);
  if trystrtoint(PortEdit.Text,port)=false then
    ShowMessage('������� ������ ����')
  else
  if ((port<1) OR (port>65535)) then
    ShowMessage('������������ �������� �����(1-65535)')
  else
  begin
    SocketC.Port:=port;
    SocketC.Address:=AddressEdit.Text;
    SocketC.Active:=True;
  end;
end;

//�������� ������ ��� �������� ����������� � �������

procedure TMainForm.SocketCConnect(Sender: TObject; Socket: TCustomWinSocket);
var
  s:TCustomWinSocket;
begin
  SocketC.Socket.SendText('#STARTADMIN#'+PasswordEdit.Text);
  StatusLabel.Caption:='�����������...';
  s:=Socket;
  with TNewThread.Create(True) do
  begin
    FreeOnTerminate:=true;
    Socket:=s;
    priority:=tpHighest;
    Resume;
  end;
end;

//�������� ������ ����� �� ������

procedure TMainForm.NewButtonClick(Sender: TObject);
var
  fs:TFileStream;
  size:integer;
begin
  OpenDialog.InitialDir := GetCurrentDir;
  if OpenDialog.Execute
  then
  begin
    if(FileInUse(OpenDialog.FileName)) then
      ShowMessage('�������� ��������� '+ OpenDialog.FileName)
    else
    begin
      fs:=TFileStream.Create(OpenDialog.FileName,fmOpenRead);
      size:=fs.size;
      fs.Free;
      SocketC.Socket.SendText('#SEND#'+ExtractFileName(OpenDialog.FileName)+'#SIZE#'+IntToStr(size)+'#VERSION#'+FileVersion(OpenDialog.FileName));
      uFile:=OpenDialog.FileName;
    end;
  end;
end;

procedure TMainForm.SocketCDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  NewButton.Enabled:=false;
  UpdateButton.Enabled:=false;
  if (Socket.ReceiveLength>0) AND (StrPos(PAnsiChar(Socket.ReceiveText),PAnsiChar('#DENY#'))<>nil) then
    ShowMessage('����������� ���������. ������ �������� ������')
  else
    ShowMessage('���������� � �������� ���������');
  StatusLabel.Caption:='���������� � �������� �� �����������';
end;

//������� ��������� ������ ��� ������� ����������

procedure TMainForm.CleanTrashTimerTimer(Sender: TObject);
var
  F:TSearchRec;
begin
  CleanTrashTimer.Enabled:=false;
  if FindFirst(AnsiReplaceText(Application.ExeName,'.exe','OLD.exe'),faAnyFile,F)=0 then
  begin
    while FileInUse(F.Name) do
    sleep(500);
    DeleteFile(F.Name);
  end;
end;

procedure TMainForm.SocketCError(Sender: TObject; Socket: TCustomWinSocket;
  ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
  if ErrorCode=10053 then
  begin
    ShowMessage('���������� � �������� ���������');
    Socket.Close;
    StatusLabel.Caption:='���������� � �������� �� �����������';
    ErrorCode:=0;
  end
  else
  if ErrorCode=10061 then
  begin
    ShowMessage('�� ������� ������������ � �������');
    ErrorCode:=0;
  end;
end;

//���������� ������������ �� ������� �����

procedure TMainForm.UpdateButtonClick(Sender: TObject);
begin
  ServerFilesWindow.FilesListBox.Items.Text:=ServerExes.Text;
  ServerFilesWindow.Visible:=true;
end;

//���������, ���������� �� ServerFilesWindow. ������������� ��� ���������� ������ �� ������� 

procedure TMainForm.sendforupdate;
var
  fs:TFileStream;
  size:integer;
begin
  OpenDialog.InitialDir := GetCurrentDir;
  if OpenDialog.Execute
  then
  begin
    if(FileInUse(OpenDialog.FileName)) then
      ShowMessage('�������� ��������� '+ OpenDialog.FileName)
    else
    begin
      fs:=TFileStream.Create(OpenDialog.FileName,fmOpenRead);
      size:=fs.size;
      fs.Free;
      SocketC.Socket.SendText('#UPDATE#'+serverFileName+'#SIZE#'+IntToStr(size)+'#VERSION#'+FileVersion(OpenDialog.FileName));
      uFile:=OpenDialog.FileName;
    end;
  end;
end;

end.
