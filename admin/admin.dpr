program admin;

uses
  Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  UpdateUnit in 'UpdateUnit.pas' {UpdateWindow},
  ServerFilesUnit in 'ServerFilesUnit.pas' {ServerFilesWindow};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TUpdateWindow, UpdateWindow);
  Application.CreateForm(TServerFilesWindow, ServerFilesWindow);
  Application.Run;
end.
