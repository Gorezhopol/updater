unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ScktComp, StrUtils, CheckLst;

type
  TMainForm = class(TForm)
    SocketO: TClientSocket;
    FileListButton: TButton;
    OpenDialog: TOpenDialog;
    KeysList: TComboBox;
    CreateButton: TButton;
    RelationCheckList: TCheckListBox;
    DeleteButton: TButton;
    AddressEdit: TEdit;
    AddressLabel: TLabel;
    PortLabel: TLabel;
    PortEdit: TEdit;
    procedure FileListButtonClick(Sender: TObject);
    procedure SocketORead(Sender: TObject; Socket: TCustomWinSocket);
    procedure CreateButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure SocketOConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure SocketODisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure RelationCheckListMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
  private
    { Private declarations }
  public
    procedure BoxUpdate;
    { Public declarations }
  end;

var
  MainForm: TMainForm;
  serverExes: TStringList;
  clientExes: TStringList;
  fs: TFileStream;
  fOldIndex: integer = -1;

implementation

{$R *.dfm}

//������ � ������� ������ ���������� � ����� ��� ���������
//������������, ���������� � ����� 'files.dat'

procedure TMainForm.FileListButtonClick(Sender: TObject);
var
  size,port: integer;
begin
  fs.Free;
  clientExes:=TStringList.Create;
  if not FileExists('files.dat') then
  begin
    fs:=TFileStream.Create('files.dat',fmCreate or fmOpenWrite);
  end
  else
  begin
    fs:=TFileStream.Create('files.dat',fmOpenWrite);
    if fs.size>0 then
    begin
      fs.Free;
      clientExes.LoadFromFile('files.dat');
      fs:=TFileStream.Create('files.dat',fmOpenWrite);
      BoxUpdate;
    end;
  end;
  if trystrtoint(PortEdit.Text,port)=false then
    ShowMessage('������� ������ ����')
  else
  if (port>65535) OR (port<1) then
    ShowMessage('������������ �������� �����(1-65535)')
  else
  try
    SocketO.Port:=port;
    SocketO.Address:=AddressEdit.text;
    SocketO.Active:=true;
  except
    ShowMessage('�������� ip-�����');
  end;
end;

//��������� ���������� � ������� �������

procedure TMainForm.SocketORead(Sender: TObject; Socket: TCustomWinSocket);
var
  str:string;
  closing:boolean;
  del:integer;
begin
  closing:=false;
  str:=Socket.ReceiveText;
  if StrPos(PAnsiChar(str),PAnsiChar('#FINISH#'))<>nil then  //����� ������
  begin
    closing:=true;
    str:=AnsiReplaceText(str,'#FINISH#','');
  end;
  if not Assigned(serverExes) then
  begin
    serverExes:=TStringList.Create;
    serverExes.Text:=AnsiReplaceText(str,'#FILE#',#13#10);
    serverExes.Delete(0);
  end
  else
  if Length(str)>0 then
  begin
    del:=serverExes.Count;
    serverExes.Text:=serverExes.text+(AnsiReplaceText(str,'#FILE#',#13#10));
    serverExes.Delete(del);
  end;
  if closing=true then
  begin
    Socket.Close;
    CreateButton.Enabled:=true;
    DeleteButton.Enabled:=true;
  end;
end;

//������� ������������ ����� ������ �� ���������� � ������ �� �������.
//��� ����������� ������������ � ���� 'files.dat', ������� ��������� ���
//������������� � ���������������� �������

procedure TMainForm.CreateButtonClick(Sender: TObject);
begin
  if KeysList.ItemIndex=-1 then
    ShowMessage('�������� �������� ��� ��� �����')
  else
  if clientExes.IndexOf(KeysList.Items.Strings[KeysList.ItemIndex]) mod 2 = 1 then
    ShowMessage('������ ���� ��� ������������')
  else
  begin
    OpenDialog.InitialDir := GetCurrentDir;
    if OpenDialog.Execute
    then
    begin
      if clientExes.IndexOf(OpenDialog.FileName) mod 2 = 0 then
        ShowMessage('��� ������� ����� ��� ������� ����')
      else
      begin
        clientExes.Add(OpenDialog.FileName);
        clientExes.Add(KeysList.Items.Strings[KeysList.ItemIndex]);
        fs.Position:=fs.size;
        if(fs.size>0) then
          fs.Write(#13#10,2);
        fs.Write(clientExes.Strings[clientExes.Count-2][1],Length(clientExes.Strings[clientExes.Count-2]));
        fs.Write(#13#10,2);
        fs.Write(clientExes.Strings[clientExes.Count-1][1],Length(clientExes.Strings[clientExes.Count-1]));
        BoxUpdate;
      end;
    end;
  end;
end;

//���������� ������ ������������

procedure TMainForm.BoxUpdate;
var
  i:integer;
begin
  RelationCheckList.Clear;
  for i:=0 to (clientExes.Count div 2)-1 do
  begin
    RelationCheckList.Items.Add(clientExes.Strings[2*i]+'->'+clientExes.Strings[2*i+1]);
  end;
end;

//�������� ������������ �� ����� 'files.dat'

procedure TMainForm.DeleteButtonClick(Sender: TObject);
var
  i: integer;
  res: boolean;
begin
  res:=false;
  for i:=RelationCheckList.Items.Count-1 downto 0 do
  begin
    if RelationCheckList.Checked[i] then
    begin
      ClientExes.Delete(2*i+1);
      ClientExes.Delete(2*i);
    end;
  end;
  fs.Free;
  while not res do
  begin
    res:=DeleteFile('files.dat');
    sleep(200);
  end;
  fs:=TFileStream.Create('files.dat',fmCreate or fmOpenWrite);
  for i:=0 to ClientExes.Count-1 do
  begin
    if(fs.size>0) then
      fs.Write(#13#10,2);
    fs.Write(clientExes.Strings[i][1],Length(clientExes.Strings[i]));
  end;
  BoxUpdate;
end;

procedure TMainForm.SocketOConnect(Sender: TObject; Socket: TCustomWinSocket);
begin
  Socket.SendText('#OPERATOR#');
end;

procedure TMainForm.SocketODisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  KeysList.Items.Text:=serverExes.Text;
end;

//������ ����� ������������ � ���� ��������� ��� ��������� ����

procedure TMainForm.RelationCheckListMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var lstIndex : Integer ;
begin
  with RelationCheckList do
  begin
   lstIndex:=SendMessage(Handle, LB_ITEMFROMPOINT, 0, MakeLParam(x,y)) ;
   if fOldIndex <> lstIndex then
     Application.CancelHint;
   fOldIndex := lstIndex;
   if (lstIndex >= 0) and (lstIndex < Items.Count) then
     Hint := Items[lstIndex]
   else
     Hint := ''
   end;
end;

end.
