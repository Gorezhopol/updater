object MainForm: TMainForm
  Left = 231
  Top = 126
  Width = 296
  Height = 208
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1082#1083#1102#1095#1077#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object AddressLabel: TLabel
    Left = 184
    Top = 24
    Width = 41
    Height = 13
    Caption = 'ip-'#1072#1076#1088#1077#1089
  end
  object PortLabel: TLabel
    Left = 184
    Top = 64
    Width = 25
    Height = 13
    Caption = #1055#1086#1088#1090
  end
  object FileListButton: TButton
    Left = 184
    Top = 104
    Width = 89
    Height = 25
    Caption = 'C'#1087#1080#1089#1086#1082' '#1092#1072#1081#1083#1086#1074
    TabOrder = 0
    OnClick = FileListButtonClick
  end
  object KeysList: TComboBox
    Left = 8
    Top = 16
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
  end
  object CreateButton: TButton
    Left = 8
    Top = 136
    Width = 129
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077
    Enabled = False
    TabOrder = 2
    OnClick = CreateButtonClick
  end
  object RelationCheckList: TCheckListBox
    Left = 8
    Top = 40
    Width = 169
    Height = 89
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnMouseMove = RelationCheckListMouseMove
  end
  object DeleteButton: TButton
    Left = 144
    Top = 136
    Width = 129
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1077
    Enabled = False
    TabOrder = 4
    OnClick = DeleteButtonClick
  end
  object AddressEdit: TEdit
    Left = 184
    Top = 40
    Width = 89
    Height = 21
    TabOrder = 5
  end
  object PortEdit: TEdit
    Left = 184
    Top = 80
    Width = 41
    Height = 21
    TabOrder = 6
  end
  object SocketO: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = SocketOConnect
    OnDisconnect = SocketODisconnect
    OnRead = SocketORead
    Left = 232
    Top = 72
  end
  object OpenDialog: TOpenDialog
    Left = 240
    Top = 8
  end
end
