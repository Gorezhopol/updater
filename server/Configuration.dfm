object ConfigWindow: TConfigWindow
  Left = 1002
  Top = 327
  BorderStyle = bsSingle
  Caption = #1050#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1103
  ClientHeight = 176
  ClientWidth = 315
  Color = clBtnFace
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PortLabel: TLabel
    Left = 140
    Top = 16
    Width = 38
    Height = 20
    Caption = #1055#1086#1088#1090':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
  end
  object AdminLabel: TLabel
    Left = 10
    Top = 48
    Width = 168
    Height = 20
    Caption = #1055#1072#1088#1086#1083#1100'['#1085#1077#1086#1073#1103#1079#1072#1090#1077#1083#1100#1085#1086']:'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
  end
  object PasswordLabel: TLabel
    Left = 68
    Top = 80
    Width = 111
    Height = 20
    Caption = #1050#1083#1080#1077#1085#1090#1089#1082#1086#1077' '#1055#1054':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
  end
  object ClientLabel: TLabel
    Left = 10
    Top = 112
    Width = 169
    Height = 20
    Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088#1089#1082#1086#1077' '#1055#1054':'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = '@Arial Unicode MS'
    Font.Style = []
    ParentFont = False
  end
  object portEdit: TEdit
    Left = 184
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object PasswordEdit: TEdit
    Left = 184
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object CreateButton: TButton
    Left = 120
    Top = 144
    Width = 75
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100
    TabOrder = 2
    OnClick = CreateButtonClick
  end
  object ClientEdit: TEdit
    Left = 184
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object AdminEdit: TEdit
    Left = 184
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 4
  end
end
