object MainForm: TMainForm
  Left = 521
  Top = 118
  Width = 656
  Height = 374
  Caption = #1057#1077#1088#1074#1077#1088
  Color = clBtnFace
  Constraints.MinHeight = 374
  Constraints.MinWidth = 656
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  DesignSize = (
    640
    336)
  PixelsPerInch = 96
  TextHeight = 13
  object DownloadLabel: TLabel
    Left = 16
    Top = 232
    Width = 72
    Height = 20
    Anchors = [akLeft, akBottom]
    Caption = #1047#1072#1075#1088#1091#1079#1082#1080':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object StartButton: TButton
    Left = 5
    Top = 304
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100
    TabOrder = 0
    OnClick = StartButtonClick
  end
  object ServerMemo: TMemo
    Left = 288
    Top = 8
    Width = 345
    Height = 321
    Anchors = [akTop, akRight, akBottom]
    ReadOnly = True
    TabOrder = 1
  end
  object ExesListBox: TListBox
    Left = 8
    Top = 32
    Width = 270
    Height = 193
    Anchors = [akLeft, akTop, akRight, akBottom]
    Constraints.MinHeight = 90
    Constraints.MinWidth = 270
    ItemHeight = 13
    TabOrder = 2
  end
  object ClientComboBox: TComboBox
    Left = 144
    Top = 8
    Width = 134
    Height = 21
    Anchors = [akLeft, akTop, akRight, akBottom]
    Constraints.MinHeight = 20
    Constraints.MinWidth = 134
    ItemHeight = 13
    TabOrder = 3
    OnSelect = ClientComboBoxSelect
  end
  object ConfigButton: TButton
    Left = 93
    Top = 304
    Width = 97
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1057#1086#1079#1076#1072#1090#1100' '#1082#1086#1085#1092#1080#1075
    TabOrder = 4
    OnClick = ConfigButtonClick
  end
  object DownloadBox: TComboBox
    Left = 96
    Top = 232
    Width = 182
    Height = 21
    Anchors = [akLeft, akRight, akBottom]
    Constraints.MinHeight = 20
    Constraints.MinWidth = 182
    Enabled = False
    ItemHeight = 13
    TabOrder = 5
  end
  object DownloadPB: TProgressBar
    Left = 8
    Top = 264
    Width = 270
    Height = 33
    Anchors = [akLeft, akTop, akRight, akBottom]
    Constraints.MinWidth = 270
    Smooth = True
    TabOrder = 6
    Visible = False
  end
  object ClearButton: TButton
    Left = 205
    Top = 304
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 7
    OnClick = ClearButtonClick
  end
  object StopButton: TButton
    Left = 5
    Top = 304
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1042#1099#1082#1083#1102#1095#1080#1090#1100
    TabOrder = 8
    Visible = False
    OnClick = StopButtonClick
  end
  object SocketS: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    ThreadCacheSize = 2048
    OnListen = SocketSListen
    OnAccept = SocketSAccept
    OnClientConnect = SocketSClientConnect
    OnClientDisconnect = SocketSClientDisconnect
    OnClientError = SocketSClientError
    Left = 16
  end
  object UpdateTimer: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = UpdateTimerTimer
    Left = 80
  end
end
