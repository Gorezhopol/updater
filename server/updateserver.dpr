program updateserver;

uses
  Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  Configuration in 'Configuration.pas' {ConfigWindow};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TConfigWindow, ConfigWindow);
  Application.Run;
end.
