unit Configuration;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TConfigWindow = class(TForm)
    portEdit: TEdit;
    PortLabel: TLabel;
    AdminLabel: TLabel;
    PasswordEdit: TEdit;
    CreateButton: TButton;
    PasswordLabel: TLabel;
    ClientLabel: TLabel;
    ClientEdit: TEdit;
    AdminEdit: TEdit;
    procedure CreateButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConfigWindow: TConfigWindow;

implementation

{$R *.dfm}

//�������� ����������������� �����

procedure TConfigWindow.CreateButtonClick(Sender: TObject);
var
  port: integer;
  fs: TFileStream;
  cfgText:TStringList;
begin
  if ((StrComp(PAnsiChar(PortEdit.Text),PAnsiChar(''))=0) OR
     (StrComp(PAnsiChar(ClientEdit.Text),PAnsiChar(''))=0) OR
     (StrComp(PAnsiChar(AdminEdit.Text),PAnsiChar(''))=0)) then
    ShowMessage('��������� ��� ����������� ����')
  else
  if trystrtoint(PortEdit.Text,port)=false then
    ShowMessage('������� ������ ����')
  else
  if (port>65535) OR (port<1) then
    ShowMessage('������������ �������� �����(1-65535)')
  else
  if ((StrComp(PAnsiChar(ExtractFileExt(ClientEdit.Text)),PAnsiChar('.exe'))<>0) OR
     (StrComp(PAnsiChar(ExtractFileExt(AdminEdit.Text)),PAnsiChar('.exe'))<>0)) then
    ShowMessage('�������� ������ �����')
  else
  try
    if FileExists('server.cfg') then
      DeleteFile('server.cfg');
    cfgText:=TStringList.Create;
    cfgText.Add('port=' + PortEdit.Text);
    cfgText.Add('password=' + PasswordEdit.Text);
    cfgText.Add('client=' + ClientEdit.Text);
    cfgText.Add('admin=' + AdminEdit.Text);
    cfgText.SaveToFile('server.cfg');
    ShowMessage('���� ������ �������');
    Visible:=false;
    Enabled:=false;
  except
    ShowMessage('���-�� ����� �� ���');
  end;
end;

end.
