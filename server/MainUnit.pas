unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, StdCtrls, WinSock, ExtCtrls, strUtils, DateUtils, syncObjs, Configuration,
  Grids, ComCtrls;

type
  TMainForm = class(TForm)
    StartButton: TButton;
    ServerMemo: TMemo;
    SocketS: TServerSocket;
    ExesListBox: TListBox;
    ClientComboBox: TComboBox;
    ConfigButton: TButton;
    DownloadBox: TComboBox;
    DownloadPB: TProgressBar;
    UpdateTimer: TTimer;
    ClearButton: TButton;
    DownloadLabel: TLabel;
    StopButton: TButton;
    procedure StartButtonClick(Sender: TObject);
    procedure SocketSClientConnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure SocketSAccept(Sender: TObject; Socket: TCustomWinSocket);
    procedure SocketSListen(Sender: TObject; Socket: TCustomWinSocket);
    procedure ClientComboBoxSelect(Sender: TObject);
    procedure SocketSClientDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ConfigButtonClick(Sender: TObject);
    procedure SocketSClientError(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure UpdateTimerTimer(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StopButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    function FileVersion(const FileName: String): String;
    procedure BoxUpdate;
    procedure updateServerExes;
    procedure ClearTrash;
    { Public declarations }
  end;
  TUserThread = class(TThread) //����� ��� ���������������� ����������
  private
    UserExes: TStringList;
    chTimeC: TStringList;
    verInfoC: TStringList;
    AppName,AppTime,AppVers: string; //������ � ���������� ����������
    procedure CleanList;
    function CompareFileVer(serv: string; user: string): integer;
    { Private declarations }
  protected
    procedure Execute; override;
    procedure BoxUpdate;       
    procedure CheckForUpdate;
  public
    S: TCustomWinSocket;
    buffer: string;
  end;
  TAdminThread = class(TThread) //����� ��� ����������������� ����������
  private
    fs: TFileStream;
    receiving: boolean;  //����� ������: ���� ������/������
    AppName,AppTime,AppVers: string; //������ �� ����������������� ����������
    flName, flVer: string;  
    flSize, flNow: integer; //������ ������������ ����� � ������� ��� ���������
    function CompareFileVer(serv: string; user: string): integer;
    procedure PBUpdate;
    procedure PBSet;
    procedure PBClear;
    { Private declarations }
  protected
    procedure Execute; override;
  public
    S: TCustomWinSocket;
    buffer: string;
  end;


var
  MainForm: TMainForm;
  ServerExes: TStringList;       //����������
  ServerExesSize: TStringList;   //� ������,
  chTimeS: TStringList;          //����������
  verInfoS: TStringList;         //�� �������
  clients: TStringList;
  authList: TStringList;
  CritSecU: TCriticalSection;
  CritSecA: TCriticalSection;
  BoxInfo: TList;
  ips: TStringList;
  password: string;        //���������,
  port: integer;           //������������
  Authorization: boolean;  //� ����� 
  clientApp: string;       //������������
  adminApp: string;        //�������
  progPath:string; // ���� � ����������, ���������� �� �������

implementation

{$R *.dfm}

// �����, �������������� ����� �������� ������ 
// ����� ����������������� � ��������� ������������

procedure TAdminThread.Execute;
var
  str,verS: string;
  list: TStringList;
  Data: PByte;
  DataLen,ind,CmprRes,i: integer;
begin
  receiving:=false;
  critSecA.Enter;
  for i:=0 to ServerExes.Count-1 do
  begin
    if ((StrComp(PAnsiChar(ServerExes.Strings[i]),PAnsiChar(clientApp))<>0)
    AND (StrComp(PAnsiChar(ServerExes.Strings[i]),PAnsiChar(adminApp))<>0)) then  
    S.SendText('#FILE#'+ServerExes.Strings[i]);        //�������� ������ ����������, ���������� �� �������              
  end;
  critSecA.Leave;
  S.SendText('#LISTFINISH#');
  while S.Connected=true do
  begin
    if S.ReceiveLength>0 then  //���� ��� ������ �����
    begin
      if not receiving then    //����� ����� ������
      begin
        str:= S.ReceiveText;   //������ ��������� � ������  
        if StrPos(PAnsiChar(str),PAnsiChar('#CURRENTAPP#'))<>nil then  //���������� � ���������� ��������������
        begin
          list:=TStringList.Create;
          str:=AnsiReplaceText(str,'#CURRENTAPP#','');
          list.Text:=AnsiReplaceText(str,'||',#13#10);
          AppName:=list.Strings[0];
          AppTime:=list.Strings[1];
          AppVers:=list.Strings[2];
          ind:=ServerExes.IndexOf(AdminApp);
          if ind<>-1 then
          begin
            CmprRes:=CompareFileVer(verInfoS.Strings[ind],AppVers);  //�������� �� ������������� ����������
            if CmprRes=1 then
              S.SendText('#UPDATECURRENTAPP#'+ ServerExesSize.Strings[ind])
            else
            if (CmprRes=0) AND (CompareDateTime(strToDateTime(chTimeS.Strings[ind]),strToDateTime(AppTime))=1) then
              S.SendText('#UPDATECURRENTAPP#'+ ServerExesSize.Strings[ind])
            else
              S.SendText('#PROCEED#');
          end
          else
            S.SendText('#PROCEED#');
        end
        else
        if StrPos(PAnsiChar(str),PAnsiChar('#READY#'))<>nil then  //�����. ���������� ������ � ����������
        begin
          fs:=TFileStream.Create(progPath+AdminApp,fmOpenRead);
          getMem(Data,fs.Size);
          fs.ReadBuffer(Data^,fs.Size);
          DataLen:=fs.size;
          S.SendBuf(Data^, DataLen);  //�������� ����������
          freeMem(Data);
          fs.Free;
        end
        else
        if StrPos(PAnsiChar(str),PAnsiChar('#SEND#'))<>nil then //�����. ���������� ����� �������� ����� ����
        begin
          list:=TStringList.Create;
          str:=AnsiReplaceText(str,'#SEND#','');
          str:=AnsiReplaceText(str,'#SIZE#',#13#10);
          list.Text:=AnsiReplaceText(str,'#VERSION#',#13#10);
          flName:=list.Strings[0];
          flVer:=list.Strings[2];
          if ((StrComp(PAnsiChar(flName),PAnsiChar(clientApp))=0) OR
             (StrComp(PAnsiChar(flName),PAnsiChar(adminApp))=0)) then
            S.SendText('#FORBIDDENNAME#')                //������������ ��� ��� ����������
          else
          if FileExists(progPath+flName) then
            S.SendText('#EXISTS#')                       //���� � ����� ������ ���������� �� �������
          else
          if FileExists(progPath+flName+'.tmp') then
            S.SendText('#ALREADYINPROGRESS#')            //��� ���������� ���� ����� � ����� �� ������
          else
          begin
            flSize:=strToInt(list.Strings[1]);
            flNow:=0;
            S.SendText('#READYTORECEIVE#');              //����� � ����� �����
            MainForm.ServerMemo.Lines.Add('Downloading file: '+flName);
            receiving:=true;
            synchronize(PBSet);
          end;
        end
        else
        if StrPos(PAnsiChar(str),PAnsiChar('#UPDATE#'))<>nil then //�����. ���������� ����� �������� ������������ ����
        begin
          list:=TStringList.Create;
          str:=AnsiReplaceText(str,'#UPDATE#','');
          str:=AnsiReplaceText(str,'#SIZE#',#13#10);
          list.Text:=AnsiReplaceText(str,'#VERSION#',#13#10);
          flName:=list.Strings[0];
          flVer:=list.Strings[2];
          VerS:=VerInfoS.Strings[serverExes.IndexOf(flName)];
          if FileExists(progPath+flName+'.tmp') then
            S.SendText('#ALREADYINPROGRESS#')            //��� ���������� ���� ����� � ����� �� ������
          else
          if CompareFileVer(VerS,flVer)=1 then
            S.SendText('#OLDVERSION#'+VerS)              //�� ������� �������� ����� ����� ������ ����� ����������
          else
          begin
            flSize:=strToInt(list.Strings[1]);
            flNow:=0;
            S.SendText('#READYTORECEIVE#');              //����� � ������ �����
            MainForm.ServerMemo.Lines.Add('Updating file: '+flName);
            receiving:=true;
            synchronize(PBSet);
          end;
        end;
      end
      else  //����� ������ ������(������)
      begin
        DataLen:=S.ReceiveLength;
        GetMem(Data,DataLen);
        DataLen:=S.ReceiveBuf(Data^,DataLen);
        if not FileExists(progPath + flName + '.tmp') then
        begin
          fs:=TFileStream.Create(progPath + flName + '.tmp', fmCreate or fmOpenWrite); //�������� ��������� ������
          fs.WriteBuffer(Data^,DataLen);
        end
        else
          fs.WriteBuffer(Data^,DataLen);
        flNow:=flNow+DataLen; 
        synchronize(PBUpdate); 
        if flNow>=flSize then  //���� ��������� ������
        begin
          MainForm.ServerMemo.Lines.Add(flName + ' downloaded');
          fs.Free;
          if FileExists(progPath + flName) then
          begin
            while DeleteFile(progPath + flName)=false do
              sleep(200);
          end;
          RenameFile(progPath + flName+'.tmp',progPath + flName);
          receiving:=false;
          synchronize(PBClear);
          MainForm.updateServerExes;
          S.SendText('#FINISH#');
        end;
        FreeMem(Data);
      end;
    end
    else
      sleep(1000);
  end;
  if FileExists(progPath + flName+'.tmp') then
  begin
    fs.Free;
    synchronize(PBClear);
    DeleteFile(progPath + flName + '.tmp');
  end;
  Terminate;
end;

//����������� ������ ������. � �������� ���������� ������������ ��������������� �������� ������.

function TAdminThread.CompareFileVer(serv: string; user:string): integer; 
var
  verInfo1, verInfo2: TStringList;
  i: integer;
begin
  Result:=0;
  verInfo1:=TStringList.Create;
  verInfo2:=TStringList.Create;
  verInfo1.Text:=AnsiReplaceText(serv,'.',#13#10);
  verInfo2.Text:=AnsiReplaceText(user,'.',#13#10);
  for i:=0 to 3 do
  begin
    if strtoint(verInfo1.Strings[i])>strtoint(verInfo2.Strings[i]) then
    begin
      Result:=1;
      break;
    end
    else
    if strtoint(verInfo1.Strings[i])<strtoint(verInfo2.Strings[i]) then
    begin
      Result:=-1;
      break;
    end;  
  end;
end;

//������������ ���������� ��� ��������� �� ��������� ������ (������ ���������, ������ ��������)

procedure TAdminThread.PBSet;
begin
  MainForm.DownloadBox.Items.Add(flName);
  if MainForm.DownloadBox.Enabled=false then
  begin
    MainForm.DownloadBox.Enabled:=true;
    MainForm.DownloadPB.Visible:=true;
    MainForm.DownloadBox.ItemIndex:=0;
  end;
end;

//���������� ������ ��������

procedure TAdminThread.PBUpdate;
begin
  if StrComp(PAnsiChar(MainForm.DownloadBox.Items.Strings[MainForm.DownloadBox.ItemIndex]),PAnsiChar(flName))=0 then
  begin
    MainForm.DownloadPB.Max:=flSize;
    MainForm.DownloadPB.Position:=flNow;
  end;
end;

procedure TAdminThread.PBClear;
begin
  MainForm.DownloadBox.Items.Delete(MainForm.DownloadBox.Items.IndexOf(flName));
  if MainForm.DownloadBox.Items.Count=0 then
  begin
    MainForm.DownloadBox.Enabled:=false;
    MainForm.DownloadPB.Visible:=false;
  end;
end;

// �����, �������������� ����� �������� ������ 
// ����� ���������������� � ��������� ������������

procedure TUserThread.Execute;
var
  i:integer;
  str:string;
  list:TStringList;
  fs: TFileStream;
  Data: PByte;
  DataLen, Sent, BufI, ind, CmprRes: Integer;
begin
  UserExes:=TStringList.Create;  // ������ � ���������������� �����������: ���
  ChTimeC:=TStringList.Create;   // ���� ���������
  VerInfoC:=TStringList.Create;  // ������
  if StrComp(PAnsiChar(buffer),PAnsiChar(''))<>0 then
  begin
    if StrPos(PAnsiChar(buffer),PAnsiChar('#CURRENTAPP#'))<>nil then //���������� � ���������������� ����������
    begin
      list:=TStringList.Create;
      buffer:=AnsiReplaceText(buffer,'#CURRENTAPP#','');
      list.Text:=AnsiReplaceText(buffer,'||',#13#10);
      AppName:=list.Strings[0];
      AppTime:=list.Strings[1];
      AppVers:=list.Strings[2];
      ind:=ServerExes.IndexOf(ClientApp);
      if ind<>-1 then
      begin
        CmprRes:=CompareFileVer(verInfoS.Strings[ind],AppVers); //��������� ������
        if CmprRes=1 then
          S.SendText('#UPDATECURRENTAPP#'+ ServerExesSize.Strings[ind])
        else
        if (CmprRes=0) AND (CompareDateTime(strToDateTime(chTimeS.Strings[ind]),strToDateTime(AppTime))=1) then
          S.SendText('#UPDATECURRENTAPP#'+ ServerExesSize.Strings[ind])
        else
          S.SendText('#PROCEED#');
      end
      else
        S.SendText('#PROCEED#');
    end
    else
    if StrPos(PAnsiChar(buffer),PAnsiChar('#STARTFILELIST#'))<>nil then //��������� � ������ ������� ������ ���������������� ����������
    begin
      buffer:=AnsiReplaceText(buffer,'#STARTFILELIST#','');
      CleanList;
    end;
    if StrPos(PAnsiChar(buffer),PAnsiChar('#FILE#'))<>nil then //������������ ���������� � ������ � ���
      begin
        list:=TStringList.Create;
        buffer:=AnsiReplaceText(buffer,'#FILE#',#13#10);
        list.Text := AnsiReplaceText(buffer,'||',#13#10);
        begin
          for i:=1 to ((list.Count-1) div 3) do
          begin
            UserExes.Add(list.Strings[3*i-2]);
            chTimeC.Add(list.Strings[3*i-1]);
            verInfoC.Add(list.Strings[3*i]);
          end;
        end;
        list.Free;
      end;
    if StrPos(PAnsiChar(buffer),PAnsiChar('#ENDFILELIST#'))<>nil then //��������� � ���������� ������, �������� �� ������������� ����������
    begin
      BoxUpdate;
      CheckForUpdate;
    end;
  end;
  while not Terminated do
  begin
    while S.ReceiveLength>0 do  //���� ��� ������ �����
      begin
        str:=S.ReceiveText;
        if StrPos(PAnsiChar(str),PAnsiChar('#CURRENTAPP#'))<>nil then //���������� � ���������������� ����������
        begin
          list:=TStringList.Create;
          str:=AnsiReplaceText(str,'#CURRENTAPP#','');
          list.Text:=AnsiReplaceText(str,'||',#13#10);
          AppName:=list.Strings[0];
          AppTime:=list.Strings[1];
          AppVers:=list.Strings[2];
          ind:=ServerExes.IndexOf(ClientApp);
          if ind<>-1 then
          begin
            CmprRes:=CompareFileVer(verInfoS.Strings[ind],AppVers);  //��������� ������
            if CmprRes=1 then
              S.SendText('#UPDATECURRENTAPP#'+ ServerExesSize.Strings[ind])
            else
            if (CmprRes=0) AND (CompareDateTime(strToDateTime(chTimeS.Strings[ind]),strToDateTime(AppTime))=1) then
              S.SendText('#UPDATECURRENTAPP#'+ ServerExesSize.Strings[ind])
            else
              S.SendText('#PROCEED#');
          end
          else
            S.SendText('#PROCEED#');
        end
        else
        if StrPos(PAnsiChar(str),PAnsiChar('#STARTFILELIST#'))<>nil then //��������� � ������ ������� ������ ���������������� ����������
        begin
          str:=AnsiReplaceText(str,'#STARTFILELIST#','');
          CleanList;
        end;
        if StrPos(PAnsiChar(str),PAnsiChar('#FILE#'))<>nil then //������������ ���������� � ������ � ���
        begin
          list:=TStringList.Create;
          str:=AnsiReplaceText(str,'#FILE#',#13#10);
          list.Text := AnsiReplaceText(str,'||',#13#10);
          begin
            for i:=1 to ((list.Count-1) div 3) do
            begin
              UserExes.Add(list.Strings[3*i-2]);
              chTimeC.Add(list.Strings[3*i-1]);
              verInfoC.Add(list.Strings[3*i]);
            end;
          end;
          list.Free;
        end;
        if StrPos(PAnsiChar(str),PAnsiChar('#ENDFILELIST#'))<>nil then //��������� � ���������� ������, �������� �� ������������� ����������
        begin
          BoxUpdate;
          CheckForUpdate;
        end;
        if StrPos(PAnsiChar(str),PAnsiChar('#READY#'))<>nil then //������ � ������� �����
        begin
          str:=AnsiReplaceText(str,'#READY#','');
          fs:=TFileStream.Create(progPath + str,fmOpenRead);
          DataLen:=fs.size;
          S.SendText('#NUMBER#'+IntToStr(DataLen));
          fs.Free;
        end
        else
        if StrPos(PAnsiChar(str),PAnsiChar('#RREADY#'))<>nil then //���������� � ����������
        begin
          str:=AnsiReplaceText(str,'#RREADY#','');
          if(StrComp(PAnsiChar(str),PAnsiChar(AppName))=0) then
            fs:=TFileStream.Create(progPath+clientApp,fmOpenRead) //���������� �������
          else
            fs:=TFileStream.Create(progPath+str,fmOpenRead);      //���������� ������ ����������
          getMem(Data,fs.Size);
          fs.ReadBuffer(Data^,fs.Size);
          DataLen:=fs.size;
          S.SendBuf(Data^, DataLen);
          freeMem(Data);
          fs.Free;
        end;
      end;
      sleep(1000);
      if S.Connected=false then
        Terminate;
    end;
end;

//������� ������ ���������������� ����������

procedure TUserThread.CleanList;
begin
  userExes.Clear;
  chTimeC.Clear;
  verInfoC.Clear;
end;

//���������� � �����������, ���������� �� �������

procedure TMainForm.updateServerExes;
var
  F:TSearchRec;
  crTime: TWin32FileAttributeData;
  sysTime,localTime: TSystemTime;
begin
  if not DirectoryExists('Progs') then
  begin
    ShowMessage('��� ����� Progs, ��������������� ��� �������� ����������');
    StopButton.Click;
  end
  else
  begin
    critSecA.Enter;
    ServerExes.Clear;
    chTimeS.Clear;
    verInfoS.Clear;
    if FindFirst(progPath + '*.exe',faAnyFile,F)=0 then
    begin
      ServerExes.Add(F.Name);
      ServerExesSize.Add(IntToStr(F.Size));
      GetFileAttributesEx(PAnsiChar(progPath+F.Name),GetFileExInfoStandard,@crTime);
      FileTimeToSystemTime(crTime.ftLastWriteTime,sysTime);
      SystemTimeToTzSpecificLocalTime(nil, sysTime, localTime);
      chTimeS.Add(DateTimeToStr(SystemTimeToDateTime(localTime)));
      verInfoS.Add(FileVersion(progPath+F.Name));
      while FindNext(F)=0 do
      begin
        ServerExes.Add(F.Name);
        ServerExesSize.Add(IntToStr(F.Size));
        GetFileAttributesEx(PAnsiChar(progPath+F.Name),GetFileExInfoStandard,@crTime);
        FileTimeToSystemTime(crTime.ftLastWriteTime,sysTime);
        SystemTimeToTzSpecificLocalTime(nil, sysTime, localTime);
        chTimeS.Add(DateTimeToStr(SystemTimeToDateTime(localTime)));
        verInfoS.Add(FileVersion(progPath+F.Name));
      end;
    end;
    critSecA.Leave;
  end;
end;

//������ ���������� � ���������������� ����������� � ListBox

procedure TUserThread.BoxUpdate;
var
  i,ind:integer;
  BoxContent: TStringList;
begin
  if MainForm.ClientComboBox.Items.IndexOf(S.RemoteAddress)=-1 then
    MainForm.ClientComboBox.Items.Add(S.RemoteAddress);
  BoxContent:=TStringList.Create;
  for i:=0 to UserExes.Count-1 do
  begin
    BoxContent.Add(UserExes.Strings[i]);
    BoxContent.Add(chTimeC.Strings[i]);
    BoxContent.Add('ver.' + verInfoC.Strings[i]);
    if i<>UserExes.Count-1 then
      BoxContent.Add('-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-')
  end;
  critSecU.Enter;
  ind:=ips.IndexOf(S.RemoteAddress);
  if ind<>-1 then
  begin
    BoxInfo.Delete(ind);
    BoxInfo.Insert(ind,BoxContent);
  end
  else
  begin
    BoxInfo.Add(BoxContent);
    ips.Add(S.RemoteAddress);
  end;  
  critSecU.Leave;
end;

//�������� �� ������������� ���������� ����������������� ����������. ��������� ������ � ���� ���������� ���������

procedure TUserThread.CheckForUpdate;
var
  i,ind,cmpRes,size:integer;
begin
  size:=0;
  for i:=0 to UserExes.Count-1 do
  begin
    ind:=ServerExes.IndexOf(UserExes.Strings[i]);
    if ind<>-1 then
    begin
      critSecA.Enter;
      cmpRes:=CompareFileVer(VerInfoS[ind],VerInfoC[i]);
      if cmpRes=0 then
      begin
        if CompareDateTime(StrToDateTime(chTimeS.Strings[ind]),StrToDateTime(chTimeC.Strings[i]))=1 then
        begin
          S.SendText('#UPDATE#'+ServerExes.Strings[ind]);
          size:=size+StrToInt(ServerExesSize.Strings[ind]);
        end;
      end
      else
      if cmpRes=1 then
      begin
        S.SendText('#UPDATE#'+ServerExes.Strings[ind]);
        size:=size+StrToInt(ServerExesSize.Strings[ind]);
      end;
      critSecA.Leave;
    end;
  end;
  if size>0 then
    S.SendText('#SIZE#'+IntToStr(size));
end;

function TUserThread.CompareFileVer(serv: string; user:string): integer;
var
  verInfo1, verInfo2: TStringList;
  i: integer;
begin
  Result:=0;
  verInfo1:=TStringList.Create;
  verInfo2:=TStringList.Create;
  verInfo1.Text:=AnsiReplaceText(serv,'.',#13#10);
  verInfo2.Text:=AnsiReplaceText(user,'.',#13#10);
  for i:=0 to 3 do
  begin
    if strtoint(verInfo1.Strings[i])>strtoint(verInfo2.Strings[i]) then
    begin
      Result:=1;
      break;
    end
    else
    if strtoint(verInfo1.Strings[i])<strtoint(verInfo2.Strings[i]) then
    begin
      Result:=-1;
      break;
    end;
  end;
end;

//����� ���������� � ���������������� �����������

procedure TMainForm.BoxUpdate;
var i:integer;
begin
  for i:=0 to SocketS.Socket.ActiveConnections-1 do
  begin
    if ClientComboBox.Items.IndexOf(SocketS.Socket.Connections[i].RemoteAddress)=-1 then
    ClientComboBox.Items.Add(SocketS.Socket.Connections[i].RemoteAddress);
  end;
  for i:=ClientComboBox.Items.Count-1 downto 0 do
  begin
    if clients.IndexOf(ClientComboBox.Items.Strings[i])=-1 then
    ClientComboBox.Items.Delete(i);
  end;
end;

function TMainForm.FileVersion(const FileName: String): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PAnsiChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PAnsiChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('%d.%d.%d.%d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS), //Minor
              HiWord(dwFileVersionLS), //Release
              LoWord(dwFileVersionLS)]); //Build
      end
      else
        Result := '0.0.0.0';
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
    Result := '0.0.0.0';
end;

//������ �������

procedure TMainForm.StartButtonClick(Sender: TObject);
var
  i:integer;
  cfg:TStringList;
begin
  if not FileExists('server.cfg') then           //����� ����� ������������
    ShowMessage('����������� ���� ������������')
  else
  begin
    progpath:=ExtractFilePath(Application.ExeName)+'progs/';
    ClearTrash;
    Authorization:=false;
    cfg:=TStringList.Create;
    cfg.LoadFromFile('server.cfg');                             //���������� ���������� �������
    port:=strtoint(AnsiReplaceText(cfg.Strings[0],'port=','')); //����
    password:=AnsiReplaceText(cfg.Strings[1],'password=','');   //������
    if StrComp(PAnsiChar(password),PAnsiChar(''))<>0 then
      Authorization:=true;
    clientApp:=AnsiReplaceText(cfg.Strings[2],'client=','');    //��� ����������������� ����������, ����������� �� �������
    adminApp:=AnsiReplaceText(cfg.Strings[3],'admin=','');      //��� ������������������ ����������, ����������� �� �������
    ips.Free;
    ServerExes.Free;
    ServerExesSize.Free;
    chTimeS.Free;
    verInfoS.Free;
    clients.free;
    authList.free;
    SocketS.Port:=port;
    SocketS.ServerType:=stNonBlocking;
    SocketS.Active:=True;
    CritSecU:=TCriticalSection.Create;
    CritSecA:=TCriticalSection.Create;
    BoxInfo:=TList.Create();
    ips:= TStringList.Create();
    chTimeS:= TStringList.Create();
    ServerExes:= TStringList.Create();
    ServerExesSize:= TStringList.Create();
    verInfoS:= TStringList.Create();
    clients:= TStringList.Create();
    authList:= TStringList.Create();
    UpdateTimer.Enabled:=true;
    StartButton.Visible:=false;
    StopButton.Visible:=true;
    UpdateServerExes;
  end;
end;

procedure TMainForm.SocketSClientConnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  ServerMemo.Lines.Add('new client: ' + Socket.RemoteAddress);
end;

//��������� ��������� �����������

procedure TMainForm.SocketSAccept(Sender: TObject; Socket: TCustomWinSocket);
var
  str: string;
  i:integer;
begin
  ServerMemo.Lines.Add('client accepted');
  if Socket.ReceiveLength<=0 then   //�������� ������������������ ��������� �� �������
  begin
    Application.ProcessMessages;
    sleep(3000);
    if Socket.ReceiveLength<=0 then //������� ����� ��������
    begin
      ServerMemo.Lines.Add('Socket timed out'); 
      Socket.Close;
    end;
  end;
  if Socket.Connected=true then
  begin
    str:=Socket.ReceiveText;
    if StrPos(PAnsiChar(str),PAnsiChar('#STARTUSER#'))<>nil then  //������ ������������ ��� ������������
    begin
      str:=AnsiReplaceText(str,'#STARTUSER#','');
      clients.Add(Socket.RemoteAddress);
      authList.Add('User');
      with TUserThread.Create(True) do     //�������� ����������������� ������ ��� ������ �������� ������
      begin
        FreeOnTerminate:=True;
        S:=Socket;
        buffer:=str;                       //������ ������� ��������� � �����
        Resume;
      end;
    end
    else
    if StrPos(PAnsiChar(str),PAnsiChar('#STARTADMIN#'))<>nil then  //������ ������������ ��� �������������
    begin
      str:=AnsiReplaceText(str,'#STARTADMIN#','');
      if (Authorization=True) AND (StrComp(PAnsiChar(str),PAnsiChar(password))<>0) then
      begin
        Socket.SendText('#DENY#');                                 //����������� ���������, �������� ������
        ServerMemo.Lines.Add('Invalid authorization: '+Socket.RemoteAddress);
        Socket.Close;
        Socket.Free;
      end
      else
      begin
        clients.Add(Socket.RemoteAddress);
        authList.Add('Admin');
        with TAdminThread.Create(True) do     //�������� ������������������ ������ ��� ������ �������� ������
        begin
          FreeOnTerminate:=True;
          S:=Socket;
          Resume;
        end;
      end;
    end
    else
    if StrPos(PAnsiChar(str),PAnsiChar('#OPERATOR#'))<>nil then  //������ ������������ ��� ��������
    begin
      clients.Add(Socket.RemoteAddress);
      authList.Add('Operator');
      for i:=0 to ServerExes.Count-1 do
      begin
        if ((StrComp(PAnsiChar(ServerExes.Strings[i]),PAnsiChar(clientApp))<>0)
        AND (StrComp(PAnsiChar(ServerExes.Strings[i]),PAnsiChar(adminApp))<>0)) then
        Socket.SendText('#FILE#'+ServerExes.Strings[i]);         //������� ������ ��������� ����������
      end;
      Socket.SendText('#FINISH#');
    end
    else
    begin
      ServerMemo.Lines.Add('Socket gave no authorization data');      //������ �� ����������� ���������������� ������
      ServerMemo.Lines.Add('������ ���������� � �������� ' + Socket.RemoteAddress);
      Socket.Close;
      Socket.Free;
    end;
  end;
end;

procedure TMainForm.SocketSListen(Sender: TObject; Socket: TCustomWinSocket);
begin
  ServerMemo.Lines.Add('Listening on port ' + inttostr(SocketS.Port));
end;

procedure TMainForm.ClientComboBoxSelect(Sender: TObject);
var i:integer;
begin
  ExesListBox.Clear;
  ExesListBox.Items:=BoxInfo.Items[ips.IndexOf(ClientComboBox.Items.Strings[ClientComboBox.ItemIndex])];
end;

procedure TMainForm.SocketSClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
  var i:integer;
begin
  i:= clients.indexof(socket.remoteAddress);
  if(i<>-1) then
  begin
    clients.Delete(i);
    authList.Delete(i);
    ClientComboBox.Items.Delete(ClientComboBox.Items.IndexOf(socket.RemoteAddress));
    ServerMemo.Lines.Add('Client disconnected: '+Socket.RemoteAddress);
  end;
end;

//�������� ���� ��� �������� ����������������� �����

procedure TMainForm.ConfigButtonClick(Sender: TObject);
begin
  ConfigWindow.Enabled:=true;
  ConfigWindow.Visible:=true;
end;

//��������� ��������� ������ � ����������

procedure TMainForm.SocketSClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
  if ErrorCode=10053 then
  begin
    Socket.Close;
    ServerMemo.Lines.Add('������ ���������� � �������� ' + Socket.RemoteAddress);
    ErrorCode:=0;
  end;
end;

//������ ��� ����������� ���������� ���������� � �����������, ���������� �� �������. �������� = 1 ���

procedure TMainForm.UpdateTimerTimer(Sender: TObject);
begin
  UpdateServerExes;
end;

procedure TMainForm.ClearButtonClick(Sender: TObject);
begin
  ServerMemo.Clear;
end;

//�������� ��������� ������, ��������� �����������

procedure TMainForm.ClearTrash;
var
  F:TSearchRec;
begin
  if FindFirst(progPath+'*.exe.tmp',faAnyFile,F)=0 then
  try
    DeleteFile(F.Name);
    while FindNext(F)=0 do
      DeleteFile(F.Name);
  except
    ShowMessage('�� ������� ������� ��������� �����, ��������� ������� �� ��� � ������ ����������');
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SocketS.Active:=false;
  ClearTrash;
end;

//��������� �������

procedure TMainForm.StopButtonClick(Sender: TObject);
var
  I:integer;
begin
  if SocketS.Socket.ActiveConnections>0 then
  begin
    for i:=0 to SocketS.Socket.ActiveConnections-1 do
      SocketS.Socket.Connections[i].Close;
  end;
  SocketS.Active:=false;
  SocketS.Close;
  ClearTrash;
  ServerMemo.Lines.Add('Server stopped');
  UpdateTimer.Enabled:=false;
  StartButton.Visible:=true;
  StopButton.Visible:=false;
end;

end.
