unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ScktComp, StdCtrls, WinSock, StrUtils, Registry, ComCtrls,ExtCtrls,
  Updater;

type
  TMainForm = class(TForm)
    SocketC: TClientSocket;
    StartButton: TButton;
    AddressEdit: TEdit;
    StartUpTimer: TTimer;
    SendExeTimer: TTimer;
    AddressLabel: TLabel;
    PortEdit: TEdit;
    PortLabel: TLabel;
    procedure StartButtonClick(Sender: TObject);
    procedure SocketCConnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StartUpTimerTimer(Sender: TObject);
    procedure SocketCError(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure SendExeTimerTimer(Sender: TObject);
    procedure SocketCDisconnect(Sender: TObject; Socket: TCustomWinSocket);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
  private
    function FileVersion(const FileName: String): String;
    { Private declarations }
  public
    procedure SendExeInformation;
    function FileInUse(FileName:string): Boolean;
    procedure Autostart;
    procedure ClearTrash;
    procedure ErrorMsg(a:string);
    { Public declarations }
  end;
 TNewThread = class(TThread)
  private
    UpdatingF: boolean;
    DataInc,DataNow: Integer;  //������ ��������� � ��������� ������ ��� ������ �����
    DataIncAll,DataNowAll: Integer;  //������ ��������� � ��������� ������ ��� ���� ������
    procedure UpdateFiles;
    procedure FormActivate;
    procedure FormDeactivate;
    procedure SetPB2;
    procedure ProgBarUpdate;
    procedure close;
    function FileVersion(const FileName: String): String;
    function getAppTime: String;
    { Private declarations }
  protected
    procedure Execute; override;
  public
    Socket: TCustomWinSocket;
  end;

var
  MainForm: TMainForm;
  Thread: TNewThread;
  uList: TStringList;
  sizeList: TStringList;
  uFile: string;
  fs:TFileStream;
  clientExes: TStringList;
  clientKeys: TStringList;

implementation

{$R *.dfm}

//�����, ���������� �� ����� ����������� ����� ������������� � ��������

procedure TNewThread.Execute;
var
  str:string;
  Buf: PByte;
  DataLen: Integer;
  n:Integer;
begin
  DataNowAll:=0;
  UpdatingF:=false;
  Socket.SendText('#STARTUSER#');
  Socket.SendText('#CURRENTAPP#'+ExtractFileName(Application.ExeName)+'||' + GetAppTime + '||' + FileVersion(Application.ExeName));  //���������� � ���������������� ����������
  while Socket.Connected=True do
  begin
    while Socket.ReceiveLength>0 do
      try
        if UpdatingF=false then  //����� ������ �������
        begin
          str:=Socket.ReceiveText;
          if(StrPos(PAnsiChar(str),PAnsiChar('#PROCEED#'))<>nil) then  //���������� �� ������� ����������. ����� ���������� ������
          begin
            MainForm.SendExeInformation;
            MainForm.SendExeTimer.Enabled:=true;
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#UPDATECURRENTAPP#'))<>nil) then  //���������� �������� ����������
          begin
            DataInc:=strtoint(AnsiReplaceText(str,'#UPDATECURRENTAPP#',''));
            DataIncAll:=DataInc;
            DataNowAll:=0;
            DataNow:=0;
            synchronize(FormActivate);
            uFile:=ExtractFileName(Application.ExeName);
            Socket.SendText('#RREADY#'+uFile);
            UpdatingF:=true;
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#SIZE#'))<>nil) then  //������ ���� ������, ����������� ��� ����������
          begin
            DataIncAll:=StrToInt(AnsiRightStr(str,Length(str)-AnsiPos('#SIZE#',str)-Length('#SIZE#')+1));
            DataNowAll:=0;
            str:=AnsiLeftStr(str,AnsiPos('#SIZE#',str)-1);
            if Assigned(uList) then
              UpdateFiles;
            synchronize(formActivate);
          end;
          if(StrPos(PAnsiChar(str),PAnsiChar('#UPDATE#'))<>nil) then  //��������� ������� ������������ ������
          begin
            if not Assigned(uList) then
            begin
              MainForm.SendExeTimer.Enabled:=false;
              uList:=TStringList.Create;
              uList.Text:=AnsiReplaceText(str,'#UPDATE#',#13#10);
              uList.Delete(0);
              if DataIncAll<>0 then
                UpdateFiles;
            end
            else
              uList.Text:=uList.text+AnsiReplaceText(str,'#UPDATE#',#13#10);
          end
          else
          if(StrPos(PAnsiChar(str),PAnsiChar('#NUMBER#'))<>nil) then  //������ ������ ����� � ������� � ����� ����������
          begin
            DataInc:=StrToInt(AnsiReplaceText(str,'#NUMBER#',''));
            DataNow:=0;
            Socket.SendText('#RREADY#'+uFile);
            uFile:=ClientExes.Strings[ClientKeys.IndexOf(uFile)];
            if (FileExists(uFile+'.tmp')) then
            begin
              while not DeleteFile(uFile+'.tmp') do
                ShowMessage('���� ' + uFile+'.tmp ������������ ������ ����������');
            end;
            UpdatingF:=true;
            synchronize(setPB2);
          end;
        end
        else      //����� ���������� ������
        begin
          n:=Socket.ReceiveLength;
          getMem(Buf,n);
          DataLen:=Socket.ReceiveBuf(Buf^,n);
          DataNow:=DataNow+n;
          DataNowAll:=DataNowAll+n;
          if (not FileExists(uFile+'.tmp')) and (DataLen>0) then
          begin
            fs:=TFileStream.Create(uFile+'.tmp',fmCreate or fmOpenWrite);
            fs.WriteBuffer(Buf^,DataLen);
          end
          else
          begin
            fs.WriteBuffer(Buf^,DataLen);
          end;
          synchronize(ProgBarUpdate);
          FreeMem(Buf,n);
          if DataNow>=DataInc then
          begin
            UpdatingF:=false;
            fs.Free;
            if(StrComp(PAnsiChar(ExtractFileName(Application.ExeName)),PAnsiChar(uFile))=0) then  //���� ����������� ���������������� ���������� ����������
            begin
              synchronize(FormDeactivate);
              RenameFile(Application.ExeName,AnsiReplaceText(Application.ExeName,'.exe','OLD.exe'));
              RenameFile(uFile+'.tmp',uFile);
              WinExec(PAnsiChar(ExtractFilePath(Application.Exename)+uFile),0);
              synchronize(Close);
              Terminate;
            end
            else                                                                            //���� ����������� ���� ����������
            begin
              while(MainForm.FileInUse(uFile)) do
              begin
                ShowMessage('�������� ��������� ' + uFile + ', ����� ��������� ����������');
                sleep(200);
              end;
              DeleteFile(uFile);
              RenameFile(uFile+'.tmp',uFile);
              uList.Delete(0);
              if uList.Count<>0 then
                UpdateFiles
              else
              begin
                FreeAndNil(ulist);
                DataIncAll:=0;
                MainForm.SendExeInformation;
                MainForm.SendExeTimer.Enabled:=true;
                synchronize(FormDeactivate);
              end;
            end;
          end;
        end;
      except
        on E:Exception do
        begin
          MainForm.ErrorMsg(E.Message);     //����� ������
        end;
      end;
    sleep(1000);
  end;
  MainForm.SendExeTimer.Enabled:=false;
  Terminate;
end;

//���� ���������� ��������� ����������������� �������

function TNewThread.GetAppTime:String;
var
  crTime: TWin32FileAttributeData;
  sysTime,localTime: TSystemTime;
begin
  GetFileAttributesEx(PAnsiChar(Application.ExeName), GetFileExInfoStandard,@crTime);
  FileTimeToSystemTime(crTime.ftLastWriteTime,sysTime);
  SystemTimeToTzSpecificLocalTime(nil, sysTime, localTime);
  Result:=DateTimeToStr(SystemTimeToDateTime(localTime));
end;

//���������� ���������� � ������ �����

function TNewThread.FileVersion(const FileName: String): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PAnsiChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PAnsiChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('%d.%d.%d.%d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS), //Minor
              HiWord(dwFileVersionLS), //Release
              LoWord(dwFileVersionLS)]); //Build
      end
      else
        Result := '0.0.0.0';
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
    Result := '0.0.0.0';
end;

function TMainForm.FileVersion(const FileName: String): String;
var
  VerInfoSize: Cardinal;
  VerValueSize: Cardinal;
  Dummy: Cardinal;
  PVerInfo: Pointer;
  PVerValue: PVSFixedFileInfo;
  iLastError: DWord;
begin
  Result := '';
  VerInfoSize := GetFileVersionInfoSize(PAnsiChar(FileName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(PVerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PAnsiChar(FileName), 0, VerInfoSize, PVerInfo) then
      begin
        if VerQueryValue(PVerInfo, '\', Pointer(PVerValue), VerValueSize) then
          with PVerValue^ do
            Result := Format('%d.%d.%d.%d', [
              HiWord(dwFileVersionMS), //Major
              LoWord(dwFileVersionMS), //Minor
              HiWord(dwFileVersionLS), //Release
              LoWord(dwFileVersionLS)]); //Build
      end
      else
        Result := '0.0.0.0';
    finally
      FreeMem(PVerInfo, VerInfoSize);
    end;
  end
  else
    Result := '0.0.0.0';
end;

//��������� ����, ������������� ������� ��������

procedure TNewThread.FormActivate;
begin
  UpdateWindow.Enabled:=true;
  UpdateWindow.Visible:=true;
  UpdateWindow.GeneralPB.Max:=DataIncAll;
  UpdateWindow.GeneralPB.Position:=0;
end;

//����������� ����, ������������� ������� ��������

procedure TNewThread.FormDeactivate;
begin
  UpdateWindow.Enabled:=false;
  UpdateWindow.Visible:=false;
  UpdateWindow.GeneralPB.Position:=0;
  UpdateWindow.IndividualPB.Position:=0;
  UpdateWindow.GeneralLabel.Caption:='';
  UpdateWindow.IndividualLabel.Caption:='';
end;

//���������� ������������ �� ���� �������� ����������

procedure TNewThread.ProgBarUpdate;
begin
  UpdateWindow.GeneralLabel.Caption:='���������� ���� ��������: ' + inttostr(round((Int(DataNowAll)/Int(DataIncAll))*100)) + '%';
  UpdateWindow.IndividualLabel.Caption:='�������� ����� ' + uFile + ': ' + inttostr(round((Int(DataNow)/Int(DataInc))*100)) + '%';
  UpdateWindow.GeneralPB.Position:=DataNowAll;
  UpdateWindow.IndividualPB.Position:=DataNow;
end;

//��������� ������������� �������� ������ �������� ��� �������� �����

procedure TNewThread.SetPB2;
begin
  UpdateWindow.IndividualPB.Max:=DataInc;
  UpdateWindow.IndividualPB.Position:=0;
end;

//�������� ������� ���������� � ����������� �������

procedure TMainForm.SendExeInformation;
var
  files: TStringList;
  F: TSearchRec;
  i: integer;
  crTime: TWin32FileAttributeData;
  sysTime,localTime: TSystemTime;
  s:string;
begin
  files:=TStringList.Create();
  files.LoadFromFile('files.dat');
  clientKeys.Clear;
  clientExes.Clear;
  for i:=0 to (files.Count div 2) - 1 do
  begin
    clientExes.Add(files.Strings[2*i]);
    clientKeys.Add(files.Strings[2*i+1]);
  end;
  files.Clear;
  for i:=0 to ClientExes.Count - 1 do
  begin
    GetFileAttributesEx(PAnsiChar(clientExes.Strings[i]), GetFileExInfoStandard,@crTime);
    FileTimeToSystemTime(crTime.ftLastWriteTime,sysTime);
    SystemTimeToTzSpecificLocalTime(nil, sysTime, localTime);
    s:=DateTimeToStr(SystemTimeToDateTime(localTime));
    files.Add(clientKeys.Strings[i] + '||' + s + '||' + FileVersion(clientExes.Strings[i]));
  end;
  SocketC.Socket.SendText('#STARTFILELIST#');
  for i:=0 to files.Count-1 do
  SocketC.Socket.SendText('#FILE#'+files.Strings[i]);
  SocketC.Socket.SendText('||#ENDFILELIST#');
  files.Free;
end;

procedure TNewThread.UpdateFiles;
begin
  uFile:=uList.Strings[0];
  Socket.SendText('#READY#'+uFile);
end;

procedure TNewThread.Close;
begin
  MainForm.Close;
end;

//���������� ������ � ������ Windows ��� ����������� �����������
//������ ��������� ��� ������� �������. �������� ������ �� ��������������.
//��� ������������� ��� ����� ����������� ������� � ��������� �������.
//��� ����� �������������� ����������������� �������,  ����� ������� ����� ������.

procedure TMainForm.Autostart;
var
  regKey: string;
begin
  regKey:='\Software\Microsoft\Windows\CurrentVersion\Run';
  with TRegistry.Create do
  try
    RootKey:=HKEY_LOCAL_MACHINE;
    OpenKey(regKey,False);
    if not ValueExists(ExtractFileName(Application.ExeName))then
    WriteString(ExtractFileName(Application.ExeName),Application.ExeName);
  finally
    Free;
  end;
end;

//�������� ����� �� �������������

function TMainForm.FileInUse(FileName: string): Boolean;
var hFileRes: HFILE;
begin
  Result := False;
  if not FileExists(FileName) then exit;
  hFileRes := CreateFile(PAnsiChar(FileName),
                                    GENERIC_READ or GENERIC_WRITE,
                                    0,
                                    nil,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,
                                    0);
  Result := (hFileRes = INVALID_HANDLE_VALUE);
  if not Result then
    CloseHandle(HFileRes);
end;

//������ ������ ������� � ������ ������ ���������� � ��������,
//���� ���������� �������������� ������ � �������

procedure TMainForm.StartButtonClick(Sender: TObject);
var
  F:TSearchRec;
  port:integer;
begin
 // Autostart;
  if FindFirst(AnsiReplaceText(Application.ExeName,'.exe','OLD.exe'),faAnyFile,F)=0 then
  begin
    while FileInUse(F.Name) do
    sleep(1000);
    DeleteFile(F.Name);
  end;
  if trystrtoint(PortEdit.Text,port)=false then
    ShowMessage('������� ������ ����')
  else
  if ((port<1) OR (port>65535)) then
    ShowMessage('������������ �������� �����(1-65535)')
  else
  begin
    clientExes.free;
    clientKeys.free;
    clientExes:=TStringList.Create;
    clientKeys:=TStringList.Create;
    SocketC.Port:=port;
    SocketC.Address:=AddressEdit.Text;
    SocketC.Active:=True;
    MainForm.Hide;
  end;
end;

//�������� ������ ��� ���������� � ��������

procedure TMainForm.SocketCConnect(Sender: TObject; Socket: TCustomWinSocket);
var
  s:TCustomWinSocket;
begin
  s:=Socket;
  with TNewThread.Create(True) do
  begin
    FreeOnTerminate:=true;
    Socket:=s;
    Priority:=tpHighest;
    Resume;
  end;
end;


procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
var
  F: TSearchRec;
begin
  SocketC.Active:=false;
  if (FileInUse(uFile+'.tmp')) then
    fs.Free;
  ClearTrash;
end;

//�������������� ������ ����������������� �������. ��������
//�������� ������ � ������ ������� ������ �� ������ � ����� �������,
//� ����� ��� ����������� � ������ �������. ��������� ��������������
//�������������� ������������ � �����������

procedure TMainForm.StartUpTimerTimer(Sender: TObject);
var
  F:TSearchRec;
  port:integer;
  data: TStringList;
begin
  StartUpTimer.Enabled:=false;
  if FindFirst(AnsiReplaceText(Application.ExeName,'.exe','OLD.exe'),faAnyFile,F)=0 then
  begin
    while FileInUse(F.Name) do
    sleep(500);
    DeleteFile(F.Name);
  end;
  ClearTrash;
  clientExes.free;
  clientKeys.free;
  clientExes:=TStringList.Create;
  clientKeys:=TStringList.Create;
  if fileExists('user.cfg') then  //����, ���������� ���������� �� ������ � ����� �������
  try
    data:=TStringList.Create;
    data.LoadFromFile('user.cfg');
    SocketC.Port:=strtoint(data.Strings[1]);
    SocketC.Address:=data.Strings[0];
    SocketC.Active:=True;
  except
    MainForm.Show;
  end
  else
    MainForm.Show;
end;

procedure TMainForm.SocketCError(Sender: TObject; Socket: TCustomWinSocket;
  ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
  if ErrorCode=10061 then
  begin
    ShowMessage('�� ������� ���������� ����������� � ��������.');
    ErrorCode:=0;
    MainForm.Show;
  end
  else
  if ErrorCode=10053 then
  begin
    ShowMessage('������ ���������� � ��������.');
    Socket.Close;
    ErrorCode:=0;
  end;
end;

//������, ���������� �� ���������� ������� ���������� � �����������,
//���������� � ������������ �� ����������

procedure TMainForm.SendExeTimerTimer(Sender: TObject);
begin
  SendExeInformation;
end;

procedure TMainForm.SocketCDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
begin
  SendExeTimer.Enabled:=false;
  MainForm.Show;
end;

//������� ��������� ������

procedure TMainForm.ClearTrash;
var
  F:TSearchRec;
begin
  if FindFirst('*.exe.tmp',faAnyFile,F)=0 then
  try
    DeleteFile(F.Name);
    while FindNext(F)=0 do
      DeleteFile(F.Name);
  except
    ShowMessage('�� ������� ������� ��������� �����, ��������� ������� �� ��� � ������ ����������');
  end;
end;

//����� ������/����������, ���������� � ������

procedure TMainForm.ErrorMsg(a:string);
begin
  ShowMessage('������: ' + a);
end;

//���������� ��������� ������� ��������� ���� ����������

procedure TMainForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  AddressEdit.Width:=round(NewWidth*(89/168));
  PortEdit.Left:=AddressEdit.Left+AddressEdit.Width+8;
  PortEdit.Width:=NewWidth-PortEdit.Left-24;
  PortLabel.Left:=PortEdit.Left;
end;

end.
