unit Updater;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TUpdateWindow = class(TForm)
    GeneralPB: TProgressBar;
    GeneralLabel: TLabel;
    IndividualPB: TProgressBar;
    IndividualLabel: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UpdateWindow: TUpdateWindow;

implementation

{$R *.dfm}

procedure TUpdateWindow.FormCreate(Sender: TObject);
begin
  EnableMenuItem( GetSystemMenu( handle, False ),SC_CLOSE, MF_BYCOMMAND or MF_GRAYED );
end;


end.
