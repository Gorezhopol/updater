program client;

uses
  Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  Updater in 'Updater.pas' {UpdateWindow};

{$R *.res}

begin

  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TUpdateWindow, UpdateWindow);
  Application.ShowMainForm:=false;
  Application.Run;
end.
