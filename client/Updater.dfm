object UpdateWindow: TUpdateWindow
  Left = 471
  Top = 157
  Width = 458
  Height = 166
  Caption = #1054#1073#1085#1086#1074#1083#1077#1085#1080#1077
  Color = clBtnFace
  Enabled = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    442
    128)
  PixelsPerInch = 96
  TextHeight = 13
  object GeneralLabel: TLabel
    Left = 8
    Top = 4
    Width = 425
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
  end
  object IndividualLabel: TLabel
    Left = 5
    Top = 56
    Width = 428
    Height = 33
    Alignment = taCenter
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object GeneralPB: TProgressBar
    Left = 8
    Top = 24
    Width = 425
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    Smooth = True
    TabOrder = 0
  end
  object IndividualPB: TProgressBar
    Left = 8
    Top = 96
    Width = 425
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    Smooth = True
    TabOrder = 1
  end
end
