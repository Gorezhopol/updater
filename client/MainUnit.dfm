object MainForm: TMainForm
  Left = 347
  Top = 146
  Width = 168
  Height = 116
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnClose = FormClose
  DesignSize = (
    152
    78)
  PixelsPerInch = 96
  TextHeight = 13
  object AddressLabel: TLabel
    Left = 8
    Top = 8
    Width = 41
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    Caption = 'ip-'#1072#1076#1088#1077#1089
  end
  object PortLabel: TLabel
    Left = 104
    Top = 8
    Width = 25
    Height = 13
    Anchors = [akLeft, akTop, akRight]
    Caption = #1055#1086#1088#1090
  end
  object StartButton: TButton
    Left = 8
    Top = 48
    Width = 137
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100#1089#1103
    TabOrder = 0
    OnClick = StartButtonClick
  end
  object AddressEdit: TEdit
    Left = 8
    Top = 24
    Width = 89
    Height = 21
    TabOrder = 1
  end
  object PortEdit: TEdit
    Left = 104
    Top = 24
    Width = 41
    Height = 21
    TabOrder = 2
  end
  object SocketC: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = SocketCConnect
    OnDisconnect = SocketCDisconnect
    OnError = SocketCError
    Left = 8
    Top = 80
  end
  object StartUpTimer: TTimer
    Interval = 2000
    OnTimer = StartUpTimerTimer
    Left = 40
    Top = 80
  end
  object SendExeTimer: TTimer
    Enabled = False
    Interval = 60000
    OnTimer = SendExeTimerTimer
    Left = 72
    Top = 80
  end
end
